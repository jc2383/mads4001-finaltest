package com.example.finalteststarter;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class AddDonutActivity extends AppCompatActivity {

    final String TAG = "DONUT";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_donut);
    }

    public void saveDonutButtonPressed(View view) {

        // @TODO: Put your code here
        Toast t = Toast.makeText(getApplicationContext(), "Donut saved!", Toast.LENGTH_SHORT);
        t.show();
    }
}
